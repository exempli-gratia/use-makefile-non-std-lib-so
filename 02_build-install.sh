#!/bin/bash

pushd $(dirname $0) > /dev/null
ABSOLUTE_PATH=$(pwd -P)
popd > /dev/null

set -x
rm -rf bin
mkdir bin
make -f Native.Makefile clean
DESTDIR=bin libdir=${ABSOLUTE_PATH}/../non-std-lib-makefile-so/lib/non-std includedir=${ABSOLUTE_PATH}/../non-std-lib-makefile-so/include/non-std make -f Native.Makefile install
set +x
