# some sample Makefile

LIBNAME := non-std-foo
EXENAME := use-non-std-libfoo-so-make

all: $(EXENAME)

install: $(EXENAME)
	# the test
	install -D -m 755 $(EXENAME) $(DESTDIR)/$(EXENAME)

# for compilation outside of cooker mode 
# e.g. native Ubuntu or SDK:
#$(EXENAME): $(libdir)/lib$(LIBNAME).so $(includedir)/$(LIBNAME).h
#	$(CC) $(EXENAME).c -o $@ -L. -L$(libdir) -l$(LIBNAME) -I$(includedir)

# for compilation in cooker mode:
# .h and .so files are already where they should be
# LD_LIBRARY_PATH=/usr/lib/non-std use-non-std-libfoo-so-make
#$(EXENAME):
#	$(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir)

# add hardcoded runpath into exe
#$(EXENAME):
#	$(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir) -Xlinker -R$(non-std-rpath)

# add relative (to exe) runpath into exe
# in the YP this expands into an absolute path:
# readelf -a use-non-std-libfoo-so-make | grep rpath
# 0x0000000f (RPATH)                      Library rpath: [/usr/lib/non-std]
$(EXENAME):
	$(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME) -L$(non-std-libdir) -I$(non-std-includedir) -Wl,-rpath,'$$ORIGIN/$(non-std-rpath-relative)'

clean:
	$(RM) $(EXENAME) *.o *.so*
