# some sample Makefile

LIBNAME := non-std-foo
EXENAME := use-non-std-libfoo-so-make

all: $(EXENAME)

install: $(EXENAME)
	# the test
	install -D -m 755 $(EXENAME) $(DESTDIR)/$(EXENAME)

# for compilation outside of cooker mode 
# e.g. native Ubuntu or SDK:
# to run e.g. on Ubuntu we need:
# LD_LIBRARY_PATH=../../non-std-lib-makefile-so/lib/non-std
# $(EXENAME): $(libdir)/lib$(LIBNAME).so $(includedir)/$(LIBNAME).h
#	$(CC) $(EXENAME).c -o $@ -L. -L$(libdir) -l$(LIBNAME) -I$(includedir)

# put runpath into the exe - absolute or relative
# -Xlinker -R/home/rber/projects/various/exempli-gratia/use-makefile-non-std-lib-so/../non-std-lib-makefile-so/lib/non-std
# 
# readelf -a use-non-std-libfoo-so-make | grep -i runpath
#  0x000000000000001d (RUNPATH)            Library runpath: [/home/rber/projects/various/exempli-gratia/use-makefile-non-std-lib-so/../non-std-lib-makefile-so/lib/non-std]
# 
# Note! if we make the runpath absolute we can move the exe around and it will still work
#
# no more LD_LIBRARY_PATH needed
$(EXENAME): $(libdir)/lib$(LIBNAME).so $(includedir)/$(LIBNAME).h
	$(CC) $(EXENAME).c -o $@ -L. -L$(libdir) -l$(LIBNAME) -I$(includedir) -Xlinker -R$(libdir)

# rpath via ORIGIN  - library relative to exe?
#$(EXENAME): $(libdir)/lib$(LIBNAME).so $(includedir)/$(LIBNAME).h
#	$(CC) $(EXENAME).c -o $@ -L. -L$(libdir) -l$(LIBNAME) -I$(includedir) -Wl,-rpath,'$$ORIGIN/../non-std-lib-makefile-so/lib/non-std'

# for compilation in cooker mode:
# .h and .so files are already where they should be
#$(EXENAME):
#	$(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME)

clean:
	$(RM) $(EXENAME) *.o *.so*
